package dev.ksarou;

import dev.ksarou.compiler.highlevel.Tokenizer;
import dev.ksarou.compiler.highlevel.domain.token.Token;
import dev.ksarou.compiler.highlevel.domain.token.Tokens;
import io.vavr.control.Try;
import org.junit.Assert;
import org.junit.Test;

import java.io.*;

public class TokenizerTest {

    private final String SOURCE = "/home/cesare/Documents/Nand2Tetris/projects/10/ArrayTest/Main.jack";

    private final String TARGET = "/home/cesare/Documents/Nand2Tetris/projects/10/ArrayTest/personal/MainT.xml";

    private final Tokenizer tokenizer = new Tokenizer();

    @Test
    public void testTokenize() throws IOException {
        BufferedReader buffer = Try.of(() -> new BufferedReader(new FileReader(SOURCE)))
                .getOrElseThrow(() -> new RuntimeException("Cant read the file"));
        Tokens tokens = tokenizer.tokenize(buffer);
        Assert.assertFalse(tokens.isEmpty());
        writeFile(tokens);
    }

    private void writeFile(Tokens tokens) throws IOException {
        File fout = new File(TARGET);
        FileOutputStream fos = new FileOutputStream(fout);
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
        bw.write("<tokens>");
        bw.newLine();
        for (Token l : tokens) {
            bw.write("<" + l.getType().getValue() + ">");
            String word = l.getWord();
            if (word.equals("<"))
                word = "&lt;";
            if (word.equals(">"))
                word = "&gt;";
            if (word.equals("\""))
                word = "&quot;";
            if (word.equals("&"))
                word = "&amp;";
            bw.write(word);
            bw.write("</" + l.getType().getValue() + ">");
            bw.newLine();
        }
        bw.write("</tokens>");
        bw.newLine();
        bw.close();
    }
}
