package dev.ksarou;

import org.junit.Assert;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest {
    @Test
    public void shouldAnswerWithTrue() {
        String path1 = "aaa/bbb/ccc/ddd.vm";
        String substring1 = path1.substring(path1.lastIndexOf("/") + 1, path1.lastIndexOf(".") > 0 ? path1.lastIndexOf(".") : path1.length());
        Assert.assertEquals("ddd", substring1);

        String path2 = "aaa/bbb/ccc/ddd";
        String substring2 = path2.substring(path2.lastIndexOf("/") + 1, path2.lastIndexOf(".") > 0 ? path2.lastIndexOf(".") : path2.length());
        Assert.assertEquals("ddd", substring2);
    }
}
