package dev.ksarou;

import dev.ksarou.compiler.highlevel.Tokenizer;
import dev.ksarou.compiler.highlevel.domain.compile.ClassCompile;
import dev.ksarou.compiler.highlevel.domain.token.Tokens;
import io.vavr.control.Try;
import org.junit.Assert;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;

public class CompilerXMLTest {

    private final Tokenizer tokenizer = new Tokenizer();

    @Test
    public void testExpressionLessSquare_main() throws IOException {
        String source = "/home/cesare/Documents/Nand2Tetris/projects/10/ExpressionLessSquare/Main.jack";
        String actual = "/home/cesare/Documents/Nand2Tetris/projects/10/ExpressionLessSquare/personal/Main.xml";
        String expected = "/home/cesare/Documents/Nand2Tetris/projects/10/ExpressionLessSquare/Main.xml";
        test(source, actual, expected);
    }

    @Test
    public void testExpressionLessSquare_square() throws IOException {
        String source = "/home/cesare/Documents/Nand2Tetris/projects/10/ExpressionLessSquare/Square.jack";
        String actual = "/home/cesare/Documents/Nand2Tetris/projects/10/ExpressionLessSquare/personal/Square.xml";
        String expected = "/home/cesare/Documents/Nand2Tetris/projects/10/ExpressionLessSquare/Square.xml";
        test(source, actual, expected);
    }

    @Test
    public void testExpressionLessSquare_squareGame() throws IOException {
        String source = "/home/cesare/Documents/Nand2Tetris/projects/10/ExpressionLessSquare/SquareGame.jack";
        String actual = "/home/cesare/Documents/Nand2Tetris/projects/10/ExpressionLessSquare/personal/SquareGame.xml";
        String expected = "/home/cesare/Documents/Nand2Tetris/projects/10/ExpressionLessSquare/SquareGame.xml";
        test(source, actual, expected);
    }

    @Test
    public void testArrayTest_main() throws IOException {
        String source = "/home/cesare/Documents/Nand2Tetris/projects/10/ArrayTest/Main.jack";
        String actual = "/home/cesare/Documents/Nand2Tetris/projects/10/ArrayTest/personal/Main.xml";
        String expected = "/home/cesare/Documents/Nand2Tetris/projects/10/ArrayTest/Main.xml";
        test(source, actual, expected);
    }

    @Test
    public void testSquare_main() throws IOException {
        String source = "/home/cesare/Documents/Nand2Tetris/projects/10/Square/Main.jack";
        String actual = "/home/cesare/Documents/Nand2Tetris/projects/10/Square/personal/Main.xml";
        String expected = "/home/cesare/Documents/Nand2Tetris/projects/10/Square/Main.xml";
        test(source, actual, expected);
    }

    @Test
    public void testSquare_square() throws IOException {
        String source = "/home/cesare/Documents/Nand2Tetris/projects/10/Square/Square.jack";
        String actual = "/home/cesare/Documents/Nand2Tetris/projects/10/Square/personal/Square.xml";
        String expected = "/home/cesare/Documents/Nand2Tetris/projects/10/Square/Square.xml";
        test(source, actual, expected);
    }

    @Test
    public void testSquare_squareGame() throws IOException {
        String source = "/home/cesare/Documents/Nand2Tetris/projects/10/Square/SquareGame.jack";
        String actual = "/home/cesare/Documents/Nand2Tetris/projects/10/Square/personal/SquareGame.xml";
        String expected = "/home/cesare/Documents/Nand2Tetris/projects/10/Square/SquareGame.xml";
        test(source, actual, expected);
    }

    @Test
    public void testSeven_main() throws IOException {
        String source = "/home/cesare/Documents/Nand2Tetris/projects/11/Seven/Main.jack";
        String actual = "/home/cesare/Documents/Nand2Tetris/projects/11/Seven/personal/Main.xml";
        ClassCompile classCompile = new ClassCompile(tokenize(source));
        write(actual, classCompile);
    }

    private void test(String source, String actual, String expected) throws IOException {
        ClassCompile classCompile = new ClassCompile(tokenize(source));
        write(actual, classCompile);
        compare(expected, actual);
    }

    private Tokens tokenize(String source) {
        BufferedReader buffer = Try.of(() -> new BufferedReader(new FileReader(source)))
                .getOrElseThrow(() -> new RuntimeException("Cant read the file"));
        return tokenizer.tokenize(buffer);
    }

    private void write(String target, ClassCompile compiler) {
        Try.run(() -> Files.write(Paths.get(target), compiler.toXML().getBytes()))
                .onFailure(Throwable::printStackTrace);
    }

    private void compare(String expected, String actual) throws IOException {
        String command = "sh /home/cesare/Documents/Nand2Tetris/tools/TextComparer.sh " + expected + " " + actual;
        Process process = Runtime.getRuntime().exec(command);
        BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream()));
        Assert.assertEquals("Comparison ended successfully", in.readLine());
    }


}
