package dev.ksarou;

import dev.ksarou.compiler.highlevel.Compiler;
import io.vavr.control.Try;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

public class CompilerTest {

    @Test
    public void testSeven_main() throws IOException {
        String source = "/home/cesare/Documents/Nand2Tetris/projects/11/Seven";
        test(source);
    }

    @Test
    public void convertToBin_main() throws IOException {
        String source = "/home/cesare/Documents/Nand2Tetris/projects/11/ConvertToBin";
        test(source);
    }

    @Test
    public void square() throws IOException {
        String source = "/home/cesare/Documents/Nand2Tetris/projects/11/Square";
        test(source);
    }

    @Test
    public void average() throws IOException {
        String source = "/home/cesare/Documents/Nand2Tetris/projects/11/Average";
        test(source);
    }

    @Test
    public void complexArrays() throws IOException {
        String source = "/home/cesare/Documents/Nand2Tetris/projects/11/ComplexArrays";
        test(source);
    }

    @Test
    public void pong() throws IOException {
        String source = "/home/cesare/Documents/Nand2Tetris/projects/11/Pong";
        test(source);
    }


    private void test(String source) throws IOException {
        Compiler compiler = new Compiler(source);
        compiler.compile();
        compareAll(source);
    }

    private void compareAll(String source) {
        File file = new File(source);
        File[] files = file.listFiles(f -> f.getName().endsWith(".vm"));
        assert files != null;
        Arrays.stream(files).forEach(f -> {
            String folder = f.getParentFile().getAbsolutePath();
            String fileName = f.getName();
            String actual = f.getAbsolutePath();
            String expected = folder + "/compare/" + fileName;
            compare(expected, actual);
        });
    }

    private void compare(String expected, String actual) {
        String successString = "Two files have same content.";
        String result = Try.of(() -> FileComparer.compare(actual, expected)).getOrElse("");
        Assert.assertEquals(result, successString);
    }
}
