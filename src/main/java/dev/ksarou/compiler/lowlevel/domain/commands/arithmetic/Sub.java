package dev.ksarou.compiler.lowlevel.domain.commands.arithmetic;

import dev.ksarou.compiler.lowlevel.domain.commands.Arithmetic;

import java.util.ArrayList;
import java.util.List;

public class Sub extends Arithmetic {

    public Sub(String line) {
        super(line);
    }

    public List<String> map() {
        List<String> mapped = new ArrayList<>();
        mapped.add("@SP");
        mapped.add("A=M-1");
        mapped.add("D=M");
        mapped.add("@SP");
        mapped.add("M=M-1");
        mapped.add("A=M-1");
        mapped.add("M=M-D");
        return mapped;
    }
}
