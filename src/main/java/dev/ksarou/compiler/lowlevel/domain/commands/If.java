package dev.ksarou.compiler.lowlevel.domain.commands;

import dev.ksarou.compiler.lowlevel.domain.Command;

import java.util.ArrayList;
import java.util.List;

public class If extends Command {

    private final String functionName;

    public If(String line, String functionName) {
        super(line);
        this.functionName = functionName;
    }

    @Override
    public List<String> map() {
        List<String> mapped = new ArrayList<>();
        mapped.add("@SP");
        mapped.add("M=M-1");
        mapped.add("A=M");
        mapped.add("D=M");
        mapped.add("@" + functionName + "$" + words.get(1));
        mapped.add("D;JNE");
        return mapped;
    }
}
