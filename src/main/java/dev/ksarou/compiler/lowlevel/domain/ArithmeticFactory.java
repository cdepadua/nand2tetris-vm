package dev.ksarou.compiler.lowlevel.domain;

import dev.ksarou.compiler.lowlevel.domain.commands.arithmetic.*;

public class ArithmeticFactory {
    public static Command command(String line) {
        if (line.contains("add"))
            return new Add(line);
        else if (line.contains("sub"))
            return new Sub(line);
        else if (line.contains("eq"))
            return new Eq(line);
        else if (line.contains("gt"))
            return new Gt(line);
        else if (line.contains("lt"))
            return new Lt(line);
        else if (line.contains("and"))
            return new And(line);
        else if (line.contains("or"))
            return new Or(line);
        else if (line.contains("neg"))
            return new Neg(line);
        else if (line.contains("not"))
            return new Not(line);
        else return null;
    }
}
