package dev.ksarou.compiler.lowlevel.domain.commands;

import dev.ksarou.compiler.lowlevel.domain.Command;

import java.util.ArrayList;
import java.util.List;

public class Pop extends Command {

    private final String fileName;

    public Pop(String line, String fileName) {
        super(line);
        this.fileName = fileName;
    }

    protected List<String> stackDecrement() {
        List<String> mapped = new ArrayList<>();
        mapped.add("@SP");
        mapped.add("M=M-1");
        return mapped;
    }

    private List<String> getStackValue() {
        List<String> mapped = new ArrayList<>();
        mapped.add("@SP");
        mapped.add("A=M");
        mapped.add("D=M");
        return mapped;
    }

    public List<String> map() {
        List<String> mapped = new ArrayList<>(stackDecrement());
        if (words.get(1).value().contains("local"))
            mapped.addAll(localMapping());
        else if (words.get(1).value().contains("argument"))
            mapped.addAll(argumentMapping());
        else if (words.get(1).value().contains("this"))
            mapped.addAll(thisMapping());
        else if (words.get(1).value().contains("that"))
            mapped.addAll(thatMapping());
        else if (words.get(1).value().contains("pointer"))
            mapped.addAll(pointerMapping());
        else if (words.get(1).value().contains("temp"))
            mapped.addAll(tempMapping());
        else if (words.get(1).value().contains("static"))
            mapped.addAll(staticMapping());
        return mapped;
    }

    private List<String> localMapping() {
        List<String> mapped = new ArrayList<>();
        mapped.add("@" + words.get(2));
        mapped.add("D=A");
        mapped.add("@R1");
        mapped.add("D=D+M");
        mapped.add("@R13");
        mapped.add("M=D");
        mapped.addAll(getStackValue());
        mapped.add("@R13");
        mapped.add("A=M");
        mapped.add("M=D");
        return mapped;
    }

    private List<String> argumentMapping() {
        List<String> mapped = new ArrayList<>();
        mapped.add("@" + words.get(2));
        mapped.add("D=A");
        mapped.add("@R2");
        mapped.add("D=D+M");
        mapped.add("@R13");
        mapped.add("M=D");
        mapped.addAll(getStackValue());
        mapped.add("@R13");
        mapped.add("A=M");
        mapped.add("M=D");
        return mapped;
    }

    private List<String> thisMapping() {
        List<String> mapped = new ArrayList<>();
        mapped.add("@" + words.get(2));
        mapped.add("D=A");
        mapped.add("@R3");
        mapped.add("D=D+M");
        mapped.add("@R13");
        mapped.add("M=D");
        mapped.addAll(getStackValue());
        mapped.add("@R13");
        mapped.add("A=M");
        mapped.add("M=D");
        return mapped;
    }

    private List<String> thatMapping() {
        List<String> mapped = new ArrayList<>();
        mapped.add("@" + words.get(2));
        mapped.add("D=A");
        mapped.add("@R4");
        mapped.add("D=D+M");
        mapped.add("@R13");
        mapped.add("M=D");
        mapped.addAll(getStackValue());
        mapped.add("@R13");
        mapped.add("A=M");
        mapped.add("M=D");
        return mapped;
    }

    private List<String> pointerMapping() {
        List<String> mapped = new ArrayList<>(getStackValue());
        mapped.add("@R" + (3 + words.get(2).toInt()));
        mapped.add("M=D");
        return mapped;
    }

    private List<String> tempMapping() {
        List<String> mapped = new ArrayList<>(getStackValue());
        mapped.add("@R" + (5 + words.get(2).toInt()));
        mapped.add("M=D");
        return mapped;
    }

    private List<String> staticMapping() {
        List<String> mapped = new ArrayList<>(getStackValue());
        mapped.add("@" + this.fileName + "." + words.get(2).toInt());
        mapped.add("M=D");
        return mapped;
    }

}
