package dev.ksarou.compiler.lowlevel.domain.commands;

import dev.ksarou.compiler.lowlevel.domain.Command;

import java.util.ArrayList;
import java.util.List;

public class Return extends Command {

    public Return(String line) {
        super(line);
    }

    @Override
    public List<String> map() {
        List<String> commands = new ArrayList<>();
        // FRAME = LCL
        commands.add("@LCL");
        commands.add("D=M");
        commands.add("@R13");
        commands.add("M=D");
        // RET = *(FRAME-5)
        commands.add("@5");
        commands.add("D=A");
        commands.add("@R13");
        commands.add("D=M-D");
        commands.add("A=D");
        commands.add("D=M");
        commands.add("@R14");
        commands.add("M=D");
        // *ARG = pop()
        commands.add("@SP");
        commands.add("M=M-1");
        commands.add("A=M");
        commands.add("D=M");
        commands.add("@ARG");
        commands.add("A=M");
        commands.add("M=D");
        // SP = ARG + 1
        commands.add("@ARG");
        commands.add("D=M+1");
        commands.add("@SP");
        commands.add("M=D");
        // THAT = *(FRAME-1)
        commands.add("@1");
        commands.add("D=A");
        commands.add("@R13");
        commands.add("D=M-D");
        commands.add("A=D");
        commands.add("D=M");
        commands.add("@THAT");
        commands.add("M=D");
        // THIS = *(FRAME-2)
        commands.add("@2");
        commands.add("D=A");
        commands.add("@R13");
        commands.add("D=M-D");
        commands.add("A=D");
        commands.add("D=M");
        commands.add("@THIS");
        commands.add("M=D");
        // ARG = *(FRAME-3)
        commands.add("@3");
        commands.add("D=A");
        commands.add("@R13");
        commands.add("D=M-D");
        commands.add("A=D");
        commands.add("D=M");
        commands.add("@ARG");
        commands.add("M=D");
        // LCL = *(FRAME-4)
        commands.add("@4");
        commands.add("D=A");
        commands.add("@R13");
        commands.add("D=M-D");
        commands.add("A=D");
        commands.add("D=M");
        commands.add("@LCL");
        commands.add("M=D");
        // goto RET
        commands.add("@R14");
        commands.add("A=M");
        commands.add("0;JMP");
        return commands;
    }
}
