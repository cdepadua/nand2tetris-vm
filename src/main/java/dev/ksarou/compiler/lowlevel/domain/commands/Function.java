package dev.ksarou.compiler.lowlevel.domain.commands;

import dev.ksarou.compiler.lowlevel.domain.Command;

import java.util.ArrayList;
import java.util.List;

public class Function extends Command {

    public Function(String line) {
        super(line);
    }

    public String functionName() {
        return this.words.get(1).value();
    }

    @Override
    public List<String> map() {
        List<String> commands = new ArrayList<>();
        commands.add("(" + this.words.get(1).value() + ")");
        for (int i = 0; i < this.words.get(2).toInt(); i++) {
            commands.add("@SP");
            commands.add("A=M");
            commands.add("M=0");
            commands.add("@SP");
            commands.add("M=M+1");
        }
        return commands;
    }
}
