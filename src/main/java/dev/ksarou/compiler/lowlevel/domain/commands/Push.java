package dev.ksarou.compiler.lowlevel.domain.commands;

import dev.ksarou.compiler.lowlevel.domain.Command;

import java.util.ArrayList;
import java.util.List;

public class Push extends Command {

    private final String fileName;

    public Push(String line, String fileName) {
        super(line);
        this.fileName = fileName;
    }

    private List<String> stackIncrement() {
        List<String> mapped = new ArrayList<>();
        mapped.add("@SP");
        mapped.add("M=M+1");
        return mapped;
    }

    private List<String> setStackValue() {
        List<String> mapped = new ArrayList<>();
        mapped.add("@SP");
        mapped.add("A=M");
        mapped.add("M=D");
        return mapped;
    }

    public List<String> map() {
        List<String> mapped = new ArrayList<>();
        if (words.get(1).value().contains("constant"))
            mapped.addAll(constantMapping());
        else if (words.get(1).value().contains("local"))
            mapped.addAll(localMapping());
        else if (words.get(1).value().contains("argument"))
            mapped.addAll(argumentMapping());
        else if (words.get(1).value().contains("this"))
            mapped.addAll(thisMapping());
        else if (words.get(1).value().contains("that"))
            mapped.addAll(thatMapping());
        else if (words.get(1).value().contains("pointer"))
            mapped.addAll(pointerMapping());
        else if (words.get(1).value().contains("temp"))
            mapped.addAll(tempMapping());
        else if (words.get(1).value().contains("static"))
            mapped.addAll(staticMapping());
        mapped.addAll(stackIncrement());
        return mapped;
    }

    private List<String> constantMapping() {
        List<String> mapped = new ArrayList<>();
        mapped.add("@" + words.get(2));
        mapped.add("D=A");
        mapped.addAll(setStackValue());
        return mapped;
    }

    private List<String> localMapping() {
        List<String> mapped = new ArrayList<>();
        mapped.add("@R1");
        mapped.add("D=M");
        mapped.add("@" + words.get(2));
        mapped.add("A=D+A");
        mapped.add("D=M");
        mapped.addAll(setStackValue());
        return mapped;
    }

    private List<String> argumentMapping() {
        List<String> mapped = new ArrayList<>();
        mapped.add("@R2");
        mapped.add("D=M");
        mapped.add("@" + words.get(2));
        mapped.add("A=D+A");
        mapped.add("D=M");
        mapped.addAll(setStackValue());
        return mapped;
    }

    private List<String> thisMapping() {
        List<String> mapped = new ArrayList<>();
        mapped.add("@R3");
        mapped.add("D=M");
        mapped.add("@" + words.get(2));
        mapped.add("A=D+A");
        mapped.add("D=M");
        mapped.addAll(setStackValue());
        return mapped;
    }

    private List<String> thatMapping() {
        List<String> mapped = new ArrayList<>();
        mapped.add("@R4");
        mapped.add("D=M");
        mapped.add("@" + words.get(2));
        mapped.add("A=D+A");
        mapped.add("D=M");
        mapped.addAll(setStackValue());
        return mapped;
    }

    private List<String> pointerMapping() {
        List<String> mapped = new ArrayList<>();
        mapped.add("@R" + (3 + words.get(2).toInt()));
        mapped.add("D=M");
        mapped.addAll(setStackValue());
        return mapped;
    }

    private List<String> tempMapping() {
        List<String> mapped = new ArrayList<>();
        mapped.add("@R" + (5 + words.get(2).toInt()));
        mapped.add("D=M");
        mapped.addAll(setStackValue());
        return mapped;
    }

    private List<String> staticMapping() {
        List<String> mapped = new ArrayList<>();
        mapped.add("@" + this.fileName + "." + words.get(2).toInt());
        mapped.add("D=M");
        mapped.addAll(setStackValue());
        return mapped;
    }

}
