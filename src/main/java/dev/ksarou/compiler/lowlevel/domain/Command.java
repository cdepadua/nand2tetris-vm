package dev.ksarou.compiler.lowlevel.domain;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public abstract class Command {

    protected List<Word> words;

    public Command(String line) {
        this.words = Arrays.stream(line.split("//")[0].split(" "))
                .map(w -> w.replaceAll(" ", ""))
                .filter(ls -> !ls.isEmpty())
                .map(Word::new)
                .collect(Collectors.toList());
    }

    public List<Word> words() {
        return this.words;
    }

    public abstract List<String> map();
}
