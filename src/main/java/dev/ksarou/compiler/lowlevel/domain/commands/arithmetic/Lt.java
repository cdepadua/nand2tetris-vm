package dev.ksarou.compiler.lowlevel.domain.commands.arithmetic;

import dev.ksarou.compiler.lowlevel.domain.commands.Arithmetic;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.ArrayList;
import java.util.List;

public class Lt extends Arithmetic {

    private final String LESSER = RandomStringUtils.randomAlphabetic(10);
    private final String NOT_LESSER = RandomStringUtils.randomAlphabetic(10);
    private final String END_LESSER = RandomStringUtils.randomAlphabetic(10);

    public Lt(String line) {
        super(line);
    }

    public List<String> map() {
        List<String> mapped = new ArrayList<>();
        mapped.add("@SP");
        mapped.add("A=M-1");
        mapped.add("D=M");
        mapped.add("@SP");
        mapped.add("M=M-1");
        mapped.add("A=M-1");
        mapped.add("D=M-D");
        mapped.add("@" + LESSER);
        mapped.add("D;JLT");
        mapped.add("@" + NOT_LESSER);
        mapped.add("D;JGE");
        mapped.add("(" + LESSER + ")");
        mapped.add("D=-1");
        mapped.add("@" + END_LESSER);
        mapped.add("0;JMP");
        mapped.add("(" + NOT_LESSER + ")");
        mapped.add("D=0");
        mapped.add("(" + END_LESSER + ")");
        mapped.add("@SP");
        mapped.add("A=M-1");
        mapped.add("M=D");
        return mapped;
    }
}
