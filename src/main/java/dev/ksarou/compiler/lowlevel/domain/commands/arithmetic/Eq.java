package dev.ksarou.compiler.lowlevel.domain.commands.arithmetic;

import dev.ksarou.compiler.lowlevel.domain.commands.Arithmetic;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.ArrayList;
import java.util.List;

public class Eq extends Arithmetic {

    private final String EQUAL = RandomStringUtils.randomAlphabetic(10);
    private final String NOT_EQUAL = RandomStringUtils.randomAlphabetic(10);
    private final String END_EQUAL = RandomStringUtils.randomAlphabetic(10);
    
    public Eq(String line) {
        super(line);
    }

    public List<String> map() {
        List<String> mapped = new ArrayList<>();
        mapped.add("@SP");
        mapped.add("A=M-1");
        mapped.add("D=M");
        mapped.add("@SP");
        mapped.add("M=M-1");
        mapped.add("A=M-1");
        mapped.add("D=D-M");
        mapped.add("@" + EQUAL);
        mapped.add("D;JEQ");
        mapped.add("@" + NOT_EQUAL);
        mapped.add("D;JNE");
        mapped.add("(" + EQUAL + ")");
        mapped.add("D=-1");
        mapped.add("@" + END_EQUAL);
        mapped.add("0;JMP");
        mapped.add("(" + NOT_EQUAL + ")");
        mapped.add("D=0");
        mapped.add("(" + END_EQUAL + ")");
        mapped.add("@SP");
        mapped.add("A=M-1");
        mapped.add("M=D");
        return mapped;
    }
}
