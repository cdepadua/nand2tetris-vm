package dev.ksarou.compiler.lowlevel.domain;

import com.google.common.collect.ImmutableList;
import dev.ksarou.compiler.lowlevel.domain.commands.*;

import java.util.ArrayList;
import java.util.List;

public class CommandFactory {

    private static final List<String> functionsName = new ArrayList<>(ImmutableList.of("Sys.init"));

    public static Command command(String line, String fileName) {
        if (line.contains("push"))
            return new Push(line, fileName);
        else if (line.contains("pop"))
            return new Pop(line, fileName);
        else if (line.startsWith("goto"))
            return new GoTo(line, functionsName.get(functionsName.size() - 1));
        else if (line.contains("if-goto"))
            return new If(line, functionsName.get(functionsName.size() - 1));
        else if (line.contains("label"))
            return new Label(line, functionsName.get(functionsName.size() - 1));
        else if (line.startsWith("function")) {
            Function function = new Function(line);
            functionsName.add(function.functionName());
            return function;
        } else if (line.startsWith("call"))
            return new Call(line);
        else if (line.startsWith("return")) {
            //functionsName.remove(functionsName.size() - 1);
            return new Return(line);
        }
        return ArithmeticFactory.command(line);
    }
}
