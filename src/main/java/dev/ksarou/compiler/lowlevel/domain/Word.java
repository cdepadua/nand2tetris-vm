package dev.ksarou.compiler.lowlevel.domain;

import java.util.Objects;

public class Word {

    private String value;

    public Word(String value) {
        this.value = value;
    }

    public String value() {
        return this.value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        if (o.getClass() != String.class && o.getClass() != Word.class) return false;
        if (o.getClass() == String.class)
            return this.value.equals(o);
        Word word = (Word) o;
        return Objects.equals(value, word.value);
    }

    @Override
    public String toString() {
        return this.value;
    }

    public int toInt() {
        return Integer.parseInt(value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
