package dev.ksarou.compiler.lowlevel.domain.commands;

import dev.ksarou.compiler.lowlevel.domain.Command;

import java.util.ArrayList;
import java.util.List;

public class Label extends Command {

    private final String functionName;

    public Label(String line, String functionName) {
        super(line);
        this.functionName = functionName;
    }

    @Override
    public List<String> map() {
        List<String> mapped = new ArrayList<>();
        mapped.add("(" + functionName + "$" + words.get(1) + ")");
        return mapped;
    }
}
