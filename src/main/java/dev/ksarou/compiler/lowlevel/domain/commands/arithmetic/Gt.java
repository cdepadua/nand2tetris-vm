package dev.ksarou.compiler.lowlevel.domain.commands.arithmetic;

import dev.ksarou.compiler.lowlevel.domain.commands.Arithmetic;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.ArrayList;
import java.util.List;

public class Gt extends Arithmetic {

    private final String GREATER = RandomStringUtils.randomAlphabetic(10);
    private final String NOT_GREATER = RandomStringUtils.randomAlphabetic(10);
    private final String END_GREATER = RandomStringUtils.randomAlphabetic(10);

    public Gt(String line) {
        super(line);
    }

    public List<String> map() {
        List<String> mapped = new ArrayList<>();
        mapped.add("@SP");
        mapped.add("A=M-1");
        mapped.add("D=M");
        mapped.add("@SP");
        mapped.add("M=M-1");
        mapped.add("A=M-1");
        mapped.add("D=M-D");
        mapped.add("@" + GREATER);
        mapped.add("D;JGT");
        mapped.add("@" + NOT_GREATER);
        mapped.add("D;JLE");
        mapped.add("(" + GREATER + ")");
        mapped.add("D=-1");
        mapped.add("@" + END_GREATER);
        mapped.add("0;JMP");
        mapped.add("(" + NOT_GREATER + ")");
        mapped.add("D=0");
        mapped.add("(" + END_GREATER + ")");
        mapped.add("@SP");
        mapped.add("A=M-1");
        mapped.add("M=D");
        return mapped;
    }
}
