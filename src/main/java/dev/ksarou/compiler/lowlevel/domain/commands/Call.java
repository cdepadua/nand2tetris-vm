package dev.ksarou.compiler.lowlevel.domain.commands;

import dev.ksarou.compiler.lowlevel.domain.Command;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.ArrayList;
import java.util.List;

public class Call extends Command {

    private final String RETURN = RandomStringUtils.randomAlphabetic(10);

    public Call(String line) {
        super(line);
    }

    @Override
    public List<String> map() {

        int nbArgs = this.words.size() == 3 ? this.words.get(2).toInt() : 0;

        List<String> commands = new ArrayList<>();
        // push return-address
        commands.add("@" + RETURN);
        commands.add("D=A");
        commands.addAll(push());
        // push LCL
        commands.add("@LCL");
        commands.add("D=M");
        commands.addAll(push());
        // push ARG
        commands.add("@ARG");
        commands.add("D=M");
        commands.addAll(push());
        // push THIS
        commands.add("@THIS");
        commands.add("D=M");
        commands.addAll(push());
        // push THAT
        commands.add("@THAT");
        commands.add("D=M");
        commands.addAll(push());
        // ARG = SP-n-5
        commands.add("@" + (nbArgs + 5));
        commands.add("D=A");
        commands.add("@SP");
        commands.add("D=M-D");
        commands.add("@ARG");
        commands.add("M=D");
        // LCL = SP
        commands.add("@SP");
        commands.add("D=M");
        commands.add("@LCL");
        commands.add("M=D");
        // goto f
        commands.add("@" + words.get(1));
        commands.add("0;JMP");
        // (return-address)
        commands.add("(" + RETURN + ")");
        return commands;
    }

    private List<String> push() {
        List<String> commands = new ArrayList<>();
        commands.add("@SP");
        commands.add("A=M");
        commands.add("M=D");
        commands.add("@SP");
        commands.add("M=M+1");
        return commands;
    }
}
