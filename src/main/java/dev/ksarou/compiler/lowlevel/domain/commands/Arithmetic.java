package dev.ksarou.compiler.lowlevel.domain.commands;

import dev.ksarou.compiler.lowlevel.domain.Command;

public abstract class Arithmetic extends Command {
    public Arithmetic(String line) {
        super(line);
    }
}
