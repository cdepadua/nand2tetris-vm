package dev.ksarou.compiler.lowlevel;

import dev.ksarou.compiler.lowlevel.domain.Command;
import dev.ksarou.compiler.lowlevel.domain.CommandFactory;
import io.vavr.control.Try;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.List;
import java.util.stream.Collectors;

public class Parser {

    private final List<Command> commands;

    public Parser(String file, String fileName) {
        BufferedReader buffer = Try.of(() -> new BufferedReader(new FileReader(file)))
                .getOrElseThrow(() -> new RuntimeException("Cant read the file"));
        this.commands = buffer.lines().filter(s -> !s.startsWith("//") && !s.equals("\n") && !s.isEmpty())
                .map(l -> CommandFactory.command(l, fileName))
                .collect(Collectors.toList());
    }

    public List<Command> commands() {
        return commands;
    }
}
