package dev.ksarou.compiler.lowlevel;

import dev.ksarou.compiler.lowlevel.domain.commands.Call;
import io.vavr.control.Try;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class VM {

    private String baseName;
    private String outputFile;
    private List<Parser> parsers = new ArrayList<>();
    private List<String> compiled = new ArrayList<>();

    public VM(String path) {
        this.baseName = path.substring(path.lastIndexOf("/") + 1, path.lastIndexOf(".") > 0 ? path.lastIndexOf(".") : path.length());
        this.outputFile = baseName + ".asm";

        File f = new File(path);
        if (f.isDirectory()) {
            File[] files = f.listFiles(file -> file.getName().endsWith(".vm"));
            assert files != null;
            this.parsers = Arrays.stream(files)
                    .map(file -> new Parser(file.getAbsolutePath(), file.getName().substring(0, file.getName().lastIndexOf("."))))
                    .collect(Collectors.toList());
        } else {
            this.parsers.add(new Parser(path, baseName));
        }
        initCompiled();
    }

    private void initCompiled() {
        compiled.add("@256");
        compiled.add("D=A");
        compiled.add("@SP");
        compiled.add("M=D");
        compiled.addAll(new Call("call Sys.init 0").map());
    }

    public void compile() {
        parsers.forEach(p -> p.commands().forEach(l -> this.compiled.addAll(l.map())));
        print();
    }

    private void print() {
        System.out.println("Printing file : " + this.outputFile);
        String data = this.compiled.stream()
                .reduce((f, s) -> f.concat("\n").concat(s)).orElseThrow(RuntimeException::new);
        Try.run(() -> Files.write(Paths.get(this.outputFile), data.getBytes()))
                .onFailure(Throwable::printStackTrace);
    }
}
