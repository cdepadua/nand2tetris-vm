package dev.ksarou.compiler.highlevel;

import com.google.common.collect.ImmutableList;
import dev.ksarou.compiler.highlevel.domain.Arithmetics;
import dev.ksarou.compiler.highlevel.domain.compile.ClassCompile;
import dev.ksarou.compiler.highlevel.domain.memory.Context;
import dev.ksarou.compiler.highlevel.domain.token.Tokens;
import io.vavr.control.Try;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Compiler {

    private final String path;
    private final Tokenizer tokenizer = new Tokenizer();
    private final List<ClassCompile> classes;

    public Compiler(String path) {
        Arithmetics.init();
        File file = new File(path);
        if (file.isDirectory()) {
            this.path = path;
            File[] files = file.listFiles(f -> f.getName().endsWith(".jack"));
            assert files != null;
            classes = Arrays.stream(files).map(f -> new ClassCompile(tokenize(f.getAbsolutePath())))
                    .collect(Collectors.toList());
        } else {
            this.path = file.getParentFile().getAbsolutePath();
            classes = ImmutableList.of(new ClassCompile(tokenize(file.getAbsolutePath())));
        }
    }

    private Tokens tokenize(String source) {
        BufferedReader buffer = Try.of(() -> new BufferedReader(new FileReader(source)))
                .getOrElseThrow(() -> new RuntimeException("Cant read the file"));
        return tokenizer.tokenize(buffer);
    }

    public void compile() {
        classes.forEach(c -> {
            write(this.path + "/" + c.getClassName() + ".vm", c.compile());
        });
    }

    private void write(String target, String text) {
        Try.run(() -> Files.write(Paths.get(target), text.getBytes()))
                .onFailure(Throwable::printStackTrace);
    }

}
