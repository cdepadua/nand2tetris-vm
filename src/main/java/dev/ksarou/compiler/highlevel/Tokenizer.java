package dev.ksarou.compiler.highlevel;

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableList;
import dev.ksarou.compiler.highlevel.domain.token.Token;
import dev.ksarou.compiler.highlevel.domain.token.TokenType;
import dev.ksarou.compiler.highlevel.domain.token.Tokens;

import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class Tokenizer {

    private static final Character WHITE_SPACE = ' ';
    private static final Character TAB = '\t';
    private static final Character STRING_DELIMITER = '"';
    private static final Character[] SYMBOLS = {'{', '}', '(', ')', '[', ']', '.',
            ',', ';', '+', '-', '*', '/', '&', '|', '<', '>', '=', '~'};
    private static final String[] KEYWORDS = {"class", "method", "function", "constructor",
            "int", "boolean", "char", "void", "var", "static", "field", "let", "do", "if",
            "else", "while", "return", "true", "false", "null", "this"};

    private boolean string = false;
    private boolean inlineComment = false;
    private boolean multilineComment = false;

    public Tokens tokenize(BufferedReader input) {
        Tokens tokens = new Tokens();
        AtomicInteger i = new AtomicInteger(1);
        input.lines().forEach(l -> {
            tokens.addAll(tokenizeLine(l.toCharArray(), 0, i.intValue()));
            i.incrementAndGet();
            inlineComment = false;
        });
        return tokens;
    }

    public Tokens tokenizeLine(char[] line, int offset, int lineNb) {
        Tokens tokens = new Tokens();
        List<Character> characters = new ArrayList<>();
        while (offset < line.length) {
            char character = line[offset];
            char lookAhead = offset + 1 < line.length ? line[offset + 1] : WHITE_SPACE;
            char lookBefore = offset - 1 > 0 ? line[offset - 1] : WHITE_SPACE;
            offset++;
            if (inAComment(character, lookAhead, lookBefore))
                break;
            if (characterIsAWhiteSpace(character) && !string) {
                if (!characters.isEmpty()) {
                    tokens.add(new Token(lineNb, (offset - characters.size()), characters, findType(characters)));
                }
                break;
            }
            if (characterIsASymbol(character) && !string) {
                if (characters.size() > 0)
                    tokens.add(new Token(lineNb, (offset - characters.size()), characters, findType(characters)));
                tokens.add(new Token(lineNb, offset, ImmutableList.of(character), findType(ImmutableList.of(character))));
                break;
            }
            if (characterIsAStringDelimiter(character)) {
                if (string || characters.size() > 0) {
                    tokens.add(new Token(lineNb, (offset - characters.size()), characters, findType(characters)));
                }
                string = !string;
                break;
            }
            characters.add(character);
        }
        if (offset < line.length)
            tokens.addAll(tokenizeLine(line, offset, lineNb));
        return tokens;
    }

    private boolean characterIsASymbol(char character) {
        return Arrays.asList(SYMBOLS).contains(character);
    }

    private boolean characterIsAWhiteSpace(char character) {
        return WHITE_SPACE.equals(character) || TAB.equals(character);
    }

    private boolean characterIsAStringDelimiter(char character) {
        return STRING_DELIMITER.equals(character);
    }

    private TokenType findType(List<Character> characters) {
        if (characters.size() == 1 && Arrays.asList(SYMBOLS).contains(characters.get(0)))
            return TokenType.SYMBOL;
        if (characters.size() > 1 && Arrays.asList(KEYWORDS).contains(Joiner.on("").join(characters)))
            return TokenType.KEYWORD;
        if (string)
            return TokenType.STRING_CONST;
        if (Character.isDigit(characters.get(0)))
            return TokenType.INT_CONST;
        return TokenType.IDENTIFIER;
    }

    private boolean inAComment(char character, char lookAhead, char lookBefore) {
        if (character == '/' && lookAhead == '*')
            multilineComment = true;
        if (character == '/' && lookBefore == '*') {
            multilineComment = false;
            return true;
        }
        if (character == '/' && lookAhead == '/')
            inlineComment = true;
        return inlineComment || multilineComment;
    }
}
