package dev.ksarou.compiler.highlevel.domain.memory;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

public class SymbolTable extends ArrayList<Variable> {

    private Integer ifCounter = null;
    private Integer whileCounter = null;

    public boolean add(String kind, String type, String name, VariableScope scope) {
        Variable variable = new Variable(kind, type, name, scope, computePosition(kind));
        return super.add(variable);
    }

    private long computePosition(String kind) {
        return this.stream().filter(s -> s.getKind().equals(kind)).count();
    }

    public boolean contains(String symbol) {
        AtomicBoolean value = new AtomicBoolean(false);
        this.forEach(v -> {
            if (v.getName().equals(symbol))
                value.set(true);
        });
        return value.get();
    }

    public Variable getByName(String symbol) {
        Variable variable = null;
        for (Variable var : this) {
            if (var.getName().equals(symbol))
                variable = var;
        }
        return variable;
    }


}
