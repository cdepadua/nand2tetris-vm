package dev.ksarou.compiler.highlevel.domain.memory;

public enum VariableScope {
    CLASS,
    SUBROUTINE;
}
