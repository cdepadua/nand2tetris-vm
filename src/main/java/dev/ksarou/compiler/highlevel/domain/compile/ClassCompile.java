package dev.ksarou.compiler.highlevel.domain.compile;

import dev.ksarou.compiler.highlevel.domain.memory.Context;
import dev.ksarou.compiler.highlevel.domain.token.Tokens;

import java.util.ArrayList;
import java.util.List;

public class ClassCompile extends NonTerminal {

    private final Context context;

    private final CompileToken keyword;
    private final CompileToken identifier;
    private final CompileToken open;
    private final List<CompileToken> body = new ArrayList<>();
    private final CompileToken close;


    public ClassCompile(Tokens tokens) {
        context = new Context(tokens.get(1).getWord());
        keyword = new Terminal(tokens, context);
        identifier = Factory.instantiate(tokens, context);
        open = Factory.instantiate(tokens, context);
        CompileToken last;
        while (!(last = Factory.instantiate(tokens, context)).toString().equals("}")) {
            body.add(last);
        }
        close = last;

//        Assert.that(keyword.toString().equals("class"), error(keyword));
//        Assert.that(identifier.getToken().getType().equals(TokenType.IDENTIFIER), error(identifier));
//        Assert.that(open.toString().equals("{"), error(open));
//        Assert.that(close.toString().equals("}"), error(open));
    }

    public String getClassName() {
        return this.identifier.getToken().getWord();
    }

    @Override
    public String toXML() {
        return "<class>\n" +
                keyword.toXML() +
                identifier.toXML() +
                open.toXML() +
                body.stream().map(CompileToken::toXML).reduce((e1, e2) -> e1 + e2).orElse("") +
                close.toXML() +
                "</class>\n";
    }

    @Override
    public String compile() {
        return body.stream().map(CompileToken::compile).reduce((s1, s2) -> s1 + s2).orElse("");
    }
}
