package dev.ksarou.compiler.highlevel.domain.compile;

import dev.ksarou.compiler.highlevel.domain.memory.Context;
import dev.ksarou.compiler.highlevel.domain.token.Tokens;

import java.util.ArrayList;
import java.util.List;

public class StatementsCompile extends NonTerminal {

    private final Context context;

    private final List<CompileToken> body = new ArrayList<>();

    StatementsCompile(Tokens tokens, Context context) {
        this.context = context;
        while (!tokens.getFirstWord().equals("}")) {
            body.add(Factory.instantiate(tokens, context));
        }
    }

    @Override
    public String toXML() {
        return "<statements>\n" +
                body.stream().map(CompileToken::toXML).reduce((e1, e2) -> e1 + e2).orElse("") +
                "</statements>\n";
    }

    @Override
    public String compile() {
        return body.stream().map(CompileToken::compile).reduce((e1, e2) -> e1 + e2).orElse("");
    }
}
