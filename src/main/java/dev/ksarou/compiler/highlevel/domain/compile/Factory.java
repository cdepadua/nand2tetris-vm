package dev.ksarou.compiler.highlevel.domain.compile;

import dev.ksarou.compiler.highlevel.domain.memory.Context;
import dev.ksarou.compiler.highlevel.domain.token.Token;
import dev.ksarou.compiler.highlevel.domain.token.TokenType;
import dev.ksarou.compiler.highlevel.domain.token.Tokens;

public class Factory {

    public static CompileToken instantiate(Tokens tokens, Context context) {
        Token firstToken = tokens.get(0);
        if (firstToken.getType().equals(TokenType.KEYWORD)) {
            String word = firstToken.getWord();
            if (word.equals("class"))
                return new ClassCompile(tokens);
            if (word.equals("function") || word.equals("constructor") || word.equals("method"))
                return new SubroutineDecCompile(tokens, context);
            if (word.equals("var"))
                return new VarDecCompile(tokens, context);
            if (word.equals("let"))
                return new LetStatementCompile(tokens, context);
            if (word.equals("do"))
                return new DoStatementCompile(tokens, context);
            if (word.equals("if"))
                return new IfStatementCompile(tokens, context);
            if (word.equals("return"))
                return new ReturnStatementCompile(tokens, context);
            if (word.equals("static") | word.equals("field"))
                return new ClassVarDecCompile(tokens, context);
            if (word.equals("while"))
                return new WhileStatementCompile(tokens, context);
        }
        return new Terminal(tokens, context);
    }

}
