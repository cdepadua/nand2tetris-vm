package dev.ksarou.compiler.highlevel.domain.token;

import com.google.common.base.Joiner;

import java.util.List;

public class Token {

    private final Integer line;
    private final Integer column;
    private final String word;
    private final TokenType type;

    public Token(Integer line, Integer column, List<Character> word, TokenType type) {
        this.line = line;
        this.column = column;
        this.word = Joiner.on("").join(word);
        this.type = type;
    }

    public Integer getLine() {
        return line;
    }

    public Integer getColumn() {
        return column;
    }

    public String getWord() {
        return word;
    }

    public TokenType getType() {
        return type;
    }

}
