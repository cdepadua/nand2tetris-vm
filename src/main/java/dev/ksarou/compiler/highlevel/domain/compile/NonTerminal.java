package dev.ksarou.compiler.highlevel.domain.compile;

import dev.ksarou.compiler.highlevel.domain.token.Token;

public abstract class NonTerminal implements CompileToken {

    protected String error(CompileToken errToken) {
        return "Unexpected token " + errToken.toString() + "line "
                + errToken.getToken().getLine() + ", column " + errToken.getToken().getColumn();
    }

    @Override
    public boolean isTerminal() {
        return false;
    }

    @Override
    public Token getToken() {
        throw new RuntimeException("This is not a terminal token !");
    }

}
