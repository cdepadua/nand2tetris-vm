package dev.ksarou.compiler.highlevel.domain.compile;

import com.google.common.collect.ImmutableList;
import dev.ksarou.compiler.highlevel.domain.memory.Context;
import dev.ksarou.compiler.highlevel.domain.token.Tokens;

class ExpressionTermCompile extends NonTerminal {
    private final Context context;
    private final CompileToken left;
    private final CompileToken expression;
    private final CompileToken right;

    public ExpressionTermCompile(Tokens tokens, Context context) {
        this.context = context;
        left = Factory.instantiate(tokens, context);
        expression = new ExpressionCompile(tokens, ImmutableList.of(")"), context);
        right = Factory.instantiate(tokens, context);
    }

    @Override
    public String toXML() {
        return "<term>\n" +
                left.toXML() +
                expression.toXML() +
                right.toXML() +
                "</term>\n";
    }

    @Override
    public String compile() {
        return expression.compile();
    }
}
