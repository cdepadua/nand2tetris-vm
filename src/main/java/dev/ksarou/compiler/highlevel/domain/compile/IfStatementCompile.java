package dev.ksarou.compiler.highlevel.domain.compile;

import com.google.common.collect.ImmutableList;
import dev.ksarou.compiler.highlevel.domain.memory.Context;
import dev.ksarou.compiler.highlevel.domain.token.Tokens;

public class IfStatementCompile extends NonTerminal {

    private final Context context;

    private final CompileToken keyword;
    private final CompileToken lefPar;
    private final CompileToken expression;
    private final CompileToken rightPar;
    private final CompileToken lefBody;
    private final CompileToken statements;
    private final CompileToken rightBody;
    private final CompileToken elseSymbol;
    private final CompileToken leftElse;
    private final CompileToken elseStatements;
    private final CompileToken rightElse;

    public IfStatementCompile(Tokens tokens, Context context) {
        this.context = context;
        keyword = new Terminal(tokens, context);
        lefPar = Factory.instantiate(tokens, context);
        expression = new ExpressionCompile(tokens, ImmutableList.of(")"), context);
        rightPar = Factory.instantiate(tokens, context);
        lefBody = Factory.instantiate(tokens, context);
        statements = new StatementsCompile(tokens, context);
        rightBody = Factory.instantiate(tokens, context);
        boolean containsElse = tokens.getFirstWord().equals("else");
        elseSymbol = containsElse ? Factory.instantiate(tokens, context) : null;
        leftElse = containsElse ? Factory.instantiate(tokens, context) : null;
        elseStatements = containsElse ? new StatementsCompile(tokens, context) : null;
        rightElse = containsElse ? Factory.instantiate(tokens, context) : null;
    }

    @Override
    public String toXML() {
        return "<ifStatement>\n" +
                keyword.toXML() +
                lefPar.toXML() +
                expression.toXML() +
                rightPar.toXML() +
                lefBody.toXML() +
                statements.toXML() +
                rightBody.toXML() +
                (elseSymbol != null ? elseSymbol.toXML() : "") +
                (leftElse != null ? leftElse.toXML() : "") +
                (elseStatements != null ? elseStatements.toXML() : "") +
                (rightElse != null ? rightElse.toXML() : "") +
                "</ifStatement>\n";
    }

    @Override
    public String compile() {
        boolean withElse = this.elseStatements != null;
        int count = this.context.getIfCounter();
        return expression.compile() +
                "if-goto IF_TRUE" + count + "\n" +
                "goto IF_FALSE" + count + "\n" +
                "label IF_TRUE" + count + "\n" +
                statements.compile() +
                (withElse ? "goto IF_END" + count + "\n" : "") +
                "label IF_FALSE" + count + "\n" +
                (withElse ? elseStatements.compile() : "") +
                (withElse ? "label IF_END" + count + "\n" : "");
    }
}
