package dev.ksarou.compiler.highlevel.domain.compile;

import dev.ksarou.compiler.highlevel.domain.memory.Context;
import dev.ksarou.compiler.highlevel.domain.token.Tokens;

import java.util.ArrayList;
import java.util.List;

public class SubroutineDecCompile extends NonTerminal {

    private final Context context;

    private final CompileToken keyword;
    private final CompileToken returnType;
    private final CompileToken identifier;
    private final CompileToken open;
    private final ParametersList arguments;
    private final CompileToken close;
    private final SubRoutineBody body;

    public SubroutineDecCompile(Tokens tokens, Context context) {
        this.context = new Context(context);
        keyword = new Terminal(tokens, this.context);
        returnType = Factory.instantiate(tokens, this.context);
        identifier = Factory.instantiate(tokens, this.context);
        open = Factory.instantiate(tokens, this.context);
        arguments = new ParametersList(tokens, this.context, keyword.getToken().getWord());
        close = Factory.instantiate(tokens, this.context);
        body = new SubRoutineBody(tokens, this.context);
    }

    @Override
    public String toXML() {
        return "<subroutineDec>\n" +
                keyword.toXML() +
                returnType.toXML() +
                identifier.toXML() +
                open.toXML() +
                arguments.toXML() +
                close.toXML() +
                body.toXML() +
                "</subroutineDec>\n";
    }

    @Override
    public String compile() {
        return "function " + context.getClassName() + "." +
                identifier.getToken().getWord() + " " +
                context.getMethodVariablesNumber() + "\n"
                + body.compile(keyword.getToken().getWord());
    }

    public static class ParametersList {

        private final List<CompileToken> parameters = new ArrayList<>();

        ParametersList(Tokens tokens, Context context, String keyword) {
            while (!tokens.get(0).getWord().equals(")")) {
                parameters.add(Factory.instantiate(tokens, context));
            }
            if(keyword.equals("method"))
                context.addToMethodSymbolTable("argument",
                        context.getClassName(),
                        "");
            for (int i = 0; i < parameters.size(); i += 3) {
                context.addToMethodSymbolTable("argument",
                        parameters.get(i).toString(),
                        parameters.get(i + 1).toString());
            }
        }

        String toXML() {
            return "<parameterList>\n" +
                    parameters.stream().map(CompileToken::toXML).reduce((e1, e2) -> e1 + e2).orElse("") +
                    "</parameterList>\n";
        }

    }

    public static class SubRoutineBody {

        private final Context context;

        private final CompileToken open;
        private final List<CompileToken> varDeclarations = new ArrayList<>();
        private final CompileToken statements;
        private final CompileToken close;

        SubRoutineBody(Tokens tokens, Context context) {
            this.context = context;
            open = Factory.instantiate(tokens, context);
            while (tokens.getFirstWord().equals("var"))
                varDeclarations.add(Factory.instantiate(tokens, context));
            statements = new StatementsCompile(tokens, context);
            close = Factory.instantiate(tokens, context);
        }

        String toXML() {
            return "<subroutineBody>\n" +
                    open.toXML() +
                    varDeclarations.stream().map(CompileToken::toXML).reduce((e1, e2) -> e1 + e2).orElse("") +
                    statements.toXML() +
                    close.toXML() +
                    "</subroutineBody>\n";
        }

        String compile(String keyword) {
            if (keyword.equals("constructor")) {
                return "push constant " + context.countClassVariables() + "\n" +
                        "call Memory.alloc 1\n" +
                        "pop pointer 0\n" +
                        statements.compile();
            }
            if (keyword.equals("method")) {
                return "push argument 0\n" +
                        "pop pointer 0\n" +
                        statements.compile();
            }
            return statements.compile();
        }
    }
}
