package dev.ksarou.compiler.highlevel.domain.memory;

public class Context {

    private String className;

    private Integer ifCounter;
    private Integer whileCounter;

    private SymbolTable classSymbolTable;
    private SymbolTable methodSymbolTable;

    public Context(String className) {
        this.className = className;
        ifCounter = null;
        whileCounter = null;
        this.classSymbolTable = new SymbolTable();
        this.methodSymbolTable = new SymbolTable();
    }

    public Context(Context context) {
        this.className = context.getClassName();
        ifCounter = null;
        whileCounter = null;
        this.classSymbolTable = context.classSymbolTable;
        this.methodSymbolTable = new SymbolTable();
    }

    public String getClassName() {
        return this.className;
    }

    public void addToClassSymbolTable(String kind, String type, String name) {
        classSymbolTable.add(kind, type, name, VariableScope.CLASS);
    }

    public void addToMethodSymbolTable(String kind, String type, String name) {
        methodSymbolTable.add(kind, type, name, VariableScope.SUBROUTINE);
    }

    public long getMethodVariablesNumber() {
        return methodSymbolTable.stream().filter(s -> s.getKind().equals("local"))
                .count();
    }

    public Variable getVariableByName(String symbol) {
        return classSymbolTable.contains(symbol) ?
                classSymbolTable.getByName(symbol) : methodSymbolTable.getByName(symbol);
    }

    public boolean isAVariable(String symbol) {
        return classSymbolTable.contains(symbol) || methodSymbolTable.contains(symbol);
    }

    public long countClassVariables() {
        return classSymbolTable.stream().filter(s -> s.getKind().equals("field")).count();
    }

    public int getIfCounter() {
        ifCounter = ifCounter == null ? 0 : ifCounter + 1;
        return ifCounter;
    }

    public int getWhileCounter() {
        whileCounter = whileCounter == null ? 0 : whileCounter + 1;
        return whileCounter;
    }
}
