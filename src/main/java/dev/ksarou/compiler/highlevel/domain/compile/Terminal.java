package dev.ksarou.compiler.highlevel.domain.compile;

import dev.ksarou.compiler.highlevel.domain.Arithmetics;
import dev.ksarou.compiler.highlevel.domain.memory.Context;
import dev.ksarou.compiler.highlevel.domain.memory.Variable;
import dev.ksarou.compiler.highlevel.domain.token.Token;
import dev.ksarou.compiler.highlevel.domain.token.TokenType;
import dev.ksarou.compiler.highlevel.domain.token.Tokens;

public class Terminal implements CompileToken {
    private final Context context;
    protected Token token;

    public Terminal(Tokens tokens, Context context) {
        this.context = context;
        this.token = tokens.shift();
    }

    public Token getToken() {
        return token;
    }

    @Override
    public String toXML() {
        String word;
        switch (token.getWord()) {
            case "<":
                word = "&lt;";
                break;
            case ">":
                word = "&gt;";
                break;
            case "\"":
                word = "&quot;";
                break;
            case "&":
                word = "&amp;";
                break;
            default:
                word = token.getWord();
                break;
        }
        return "<" + token.getType().getValue() + ">" + word + "</" + token.getType().getValue() + ">\n";
    }

    @Override
    public String compile() {
        //Compile Arithmetics operations
        if (token.getType().equals(TokenType.SYMBOL)) {
            if (Arithmetics.isArithmetic(this))
                return Arithmetics.compile(this) + "\n";
            if (token.getWord().equals(",") || token.getWord().equals(";"))
                return "";
            //Compile dot
            return token.getWord();
        }
        //Compile numeric constants
        if (token.getType().equals(TokenType.INT_CONST))
            return "push constant " + token.getWord() + "\n";
        if (token.getType().equals(TokenType.STRING_CONST)) {
            String string = token.getWord();
            String result = "push constant " + string.length() + "\n" + "call String.new 1\n";
            for (int i = 0; i < string.length(); i++) {
                result += "push constant " + ((int) string.charAt(i)) + "\n" +
                        "call String.appendChar 2\n";
            }
            return result;
        }
        if (token.getType().equals(TokenType.IDENTIFIER)) {
            // compile variables
            if (context.isAVariable(token.getWord())) {
                Variable variable = context.getVariableByName(token.getWord());
                String kind = variable.getKind().equals("field") ? "this" : variable.getKind();
                return kind + " " + variable.getPosition();
            }
            return token.getWord();
        }
        if (token.getType().equals(TokenType.KEYWORD)) {
            if (token.getWord().equals("this")) {
                return "push pointer 0\n";
            }
            if (token.getWord().equals("true")) {
                return "push constant 0\n" +
                        "not\n";
            }
            if (token.getWord().equals("false") || token.getWord().equals("null")) {
                return "push constant 0\n";
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return token.getWord();
    }

    @Override
    public boolean isTerminal() {
        return true;
    }
}
