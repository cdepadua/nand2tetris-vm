package dev.ksarou.compiler.highlevel.domain.compile;

import com.google.common.collect.ImmutableList;
import dev.ksarou.compiler.highlevel.domain.memory.Context;
import dev.ksarou.compiler.highlevel.domain.token.Tokens;

class ArrayTermCompile extends NonTerminal {

    private final Context context;

    private final CompileToken identifier;
    private final CompileToken left;
    private final CompileToken expression;
    private final CompileToken right;

    public ArrayTermCompile(Tokens tokens, Context context) {
        this.context = context;
        identifier = Factory.instantiate(tokens, context);
        left = Factory.instantiate(tokens, context);
        expression = new ExpressionCompile(tokens, ImmutableList.of("]"), context);
        right = Factory.instantiate(tokens, context);
    }

    @Override
    public String toXML() {
        return "<term>\n" +
                identifier.toXML() +
                left.toXML() +
                expression.toXML() +
                right.toXML() +
                "</term>\n";
    }

    @Override
    public String compile() {
        return expression.compile() +
                "push " + identifier.compile() + "\n" +
                "add\n" +
                "pop pointer 1\n" +
                "push that 0\n";
    }
}
