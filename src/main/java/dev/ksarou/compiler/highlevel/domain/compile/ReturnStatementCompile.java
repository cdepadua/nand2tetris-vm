package dev.ksarou.compiler.highlevel.domain.compile;

import com.google.common.collect.ImmutableList;
import dev.ksarou.compiler.highlevel.domain.memory.Context;
import dev.ksarou.compiler.highlevel.domain.token.Tokens;

public class ReturnStatementCompile extends NonTerminal {

    private final Context context;

    private final CompileToken keyword;
    private final CompileToken expression;
    private final CompileToken end;

    public ReturnStatementCompile(Tokens tokens, Context context) {
        this.context = context;
        keyword = new Terminal(tokens, context);
        boolean endSymbol = tokens.get(0).getWord().equals(";");
        expression = !endSymbol ? new ExpressionCompile(tokens, ImmutableList.of(";"), context) : null;
        end = Factory.instantiate(tokens, context);
    }

    @Override
    public String toXML() {
        return "<returnStatement>\n" +
                keyword.toXML() +
                (expression != null ? expression.toXML() : "") +
                end.toXML() +
                "</returnStatement>\n";
    }

    @Override
    public String compile() {
        String result = "";
        if (expression == null)
            result += "push constant 0\n";
        else
            result += expression.compile();
        result += "return\n";
        return result;
    }
}
