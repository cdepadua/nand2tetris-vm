package dev.ksarou.compiler.highlevel.domain.compile;

import com.google.common.collect.ImmutableList;
import dev.ksarou.compiler.highlevel.domain.memory.Context;
import dev.ksarou.compiler.highlevel.domain.token.Tokens;

import java.util.ArrayList;
import java.util.List;

public class ExpressionsListCompile extends NonTerminal {

    private final Context context;

    private final List<CompileToken> expressionList = new ArrayList<>();

    ExpressionsListCompile(Tokens tokens, Context context) {
        this.context = context;
        boolean needColumn = false;
        while (!tokens.getFirstWord().equals(")")) {
            if (needColumn) {
                expressionList.add(Factory.instantiate(tokens, context));
            }
            expressionList.add(new ExpressionCompile(tokens, ImmutableList.of(")", ","), context));
            needColumn = true;
        }
    }

    public long count() {
        return this.expressionList.stream().filter(e -> e instanceof ExpressionCompile).count();
    }

    @Override
    public String toXML() {
        return "<expressionList>\n" +
                expressionList.stream().map(CompileToken::toXML).reduce((e1, e2) -> e1 + e2).orElse("") +
                "</expressionList>\n";
    }

    @Override
    public String compile() {
        return expressionList.stream().map(CompileToken::compile).reduce((e1, e2) -> e1 + e2).orElse("");
    }
}
