package dev.ksarou.compiler.highlevel.domain.compile;

import dev.ksarou.compiler.highlevel.domain.memory.Context;
import dev.ksarou.compiler.highlevel.domain.token.Token;

public interface CompileToken {

    boolean isTerminal();

    Token getToken();

    String toXML();

    String compile();
}
