package dev.ksarou.compiler.highlevel.domain.compile;

import dev.ksarou.compiler.highlevel.domain.memory.Context;
import dev.ksarou.compiler.highlevel.domain.token.Token;
import dev.ksarou.compiler.highlevel.domain.token.TokenType;
import dev.ksarou.compiler.highlevel.domain.token.Tokens;

import java.util.ArrayList;
import java.util.List;

public class ExpressionCompile extends NonTerminal {

    private final Context context;

    private final List<CompileToken> words = new ArrayList<>();

    ExpressionCompile(Tokens tokens, List<String> expressionEnd, Context context) {
        this.context = context;
        boolean firstTour = true;
        while (!expressionEnd.contains(tokens.getFirstWord())) {
            this.words.add(compileToken(tokens, context, firstTour));
            firstTour = false;
        }
    }

    private static CompileToken compileToken(Tokens tokens, Context context, boolean firstTour) {
        Token firstToken = tokens.getFirstToken();
        String lookAhead = tokens.getLookAhead().getWord();
        if (firstToken.getType().equals(TokenType.SYMBOL)) {
            if (firstToken.getWord().equals("("))
                return new ExpressionTermCompile(tokens, context);
            if ((firstToken.getWord().equals("+") ||
                    firstToken.getWord().equals("-") ||
                    firstToken.getWord().equals("~"))
                    && firstTour)
                return new SignedTermCompile(tokens, context);
            return Factory.instantiate(tokens, context);
        } else {
            if (firstToken.getType().equals(TokenType.IDENTIFIER) || firstToken.getType().equals(TokenType.KEYWORD)) {
                if (lookAhead.equals("["))
                    return new ArrayTermCompile(tokens, context);
                if (lookAhead.equals(".") || lookAhead.equals("("))
                    return new SubroutineTermCompile(tokens, context);
            }
            return new TermCompile(tokens, context);
        }
    }

    @Override
    public String toXML() {
        return "<expression>\n" +
                words.stream().map(CompileToken::toXML).reduce((e1, e2) -> e1 + e2).orElse("") +
                "</expression>\n";
    }

    @Override
    public String compile() {
        List<CompileToken> operators = new ArrayList<>();
        StringBuilder result = new StringBuilder();
        for (CompileToken t : words) {
            if (t.isTerminal() && t.getToken().getType().equals(TokenType.SYMBOL))
                operators.add(t);
            else
                result.append(t.compile());
        }
        for (CompileToken o : operators) {
            result.append(o.compile());
        }
        return result.toString();
    }
}
