package dev.ksarou.compiler.highlevel.domain.compile;

import dev.ksarou.compiler.highlevel.domain.Arithmetics;
import dev.ksarou.compiler.highlevel.domain.memory.Context;
import dev.ksarou.compiler.highlevel.domain.token.Tokens;

public class SignedTermCompile extends NonTerminal {

    private final Context context;

    private final CompileToken sign;
    private final CompileToken term;

    public SignedTermCompile(Tokens tokens, Context context) {
        this.context = context;
        sign = Factory.instantiate(tokens, context);
        term = tokens.getFirstWord().equals("(") ?
                new ExpressionTermCompile(tokens, context) : new TermCompile(tokens, context);
    }

    @Override
    public String toXML() {
        return "<term>\n" +
                sign.toXML() +
                term.toXML() +
                "</term>\n";
    }

    @Override
    public String compile() {
        return term.compile() +
                Arithmetics.compile(sign, true) + "\n";
    }
}
