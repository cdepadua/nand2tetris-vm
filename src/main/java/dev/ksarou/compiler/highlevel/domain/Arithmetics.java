package dev.ksarou.compiler.highlevel.domain;

import dev.ksarou.compiler.highlevel.domain.compile.CompileToken;

import java.util.HashMap;

public class Arithmetics {

    private static final HashMap<String, String> arithmetics = new HashMap<>();

    public static void init() {
        arithmetics.put("=", "eq");
        arithmetics.put("+", "add");
        arithmetics.put("-", "sub");
        arithmetics.put("*", "call Math.multiply 2");
        arithmetics.put("/", "call Math.divide 2");
        arithmetics.put("~", "not");
        arithmetics.put("&", "and");
        arithmetics.put("|", "or");
        arithmetics.put("<", "lt");
        arithmetics.put(">", "gt");
    }

    public static boolean isArithmetic(CompileToken symbol) {
        return arithmetics.containsKey(symbol.getToken().getWord());
    }

    public static String compile(CompileToken symbol, boolean signed) {
        if (symbol.getToken().getWord().equals("-") && signed)
            return "neg";
        return arithmetics.get(symbol.getToken().getWord());
    }

    public static String compile(CompileToken symbol) {
        return arithmetics.get(symbol.getToken().getWord());
    }
}
