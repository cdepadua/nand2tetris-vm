package dev.ksarou.compiler.highlevel.domain.token;

import java.util.ArrayList;

public class Tokens extends ArrayList<Token> {

    public Token shift() {
        return this.remove(0);
    }

    public String getFirstWord() {
        return this.get(0).getWord();
    }

    public Token getFirstToken() {
        return this.get(0);
    }

    public Token getLookAhead() {
        return this.get(1);
    }

    public Token pop() {
        return this.remove(this.size() - 1);
    }

}
