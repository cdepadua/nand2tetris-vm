package dev.ksarou.compiler.highlevel.domain.compile;

import dev.ksarou.compiler.highlevel.Compiler;
import dev.ksarou.compiler.highlevel.domain.memory.Context;
import dev.ksarou.compiler.highlevel.domain.token.Tokens;

import java.util.ArrayList;
import java.util.List;

public class VarDecCompile extends NonTerminal {

    private final Context context;
    private final CompileToken keyword;
    private final CompileToken kind;
    private final List<CompileToken> tokens = new ArrayList<>();

    public VarDecCompile(Tokens tokens, Context context) {
        this.context = context;
        keyword = new Terminal(tokens, context);
        kind = new Terminal(tokens, context);
        boolean varDec = true;
        while (varDec) {
            boolean semiColumn = tokens.getFirstWord().equals(";");
            if (semiColumn)
                varDec = false;
            this.tokens.add(Factory.instantiate(tokens, context));
        }
        for (int i = 0; i < this.tokens.size(); i += 2) {
            context.addToMethodSymbolTable("local",
                    kind.toString(),
                    this.tokens.get(i).toString());
        }
    }

    public List<CompileToken> getTokens() {
        return tokens;
    }

    @Override
    public String toXML() {
        return "<varDec>\n" +
                keyword.toXML() +
                kind.toXML() +
                tokens.stream().map(CompileToken::toXML).reduce((e1, e2) -> e1 + e2).orElse("") +
                "</varDec>\n";
    }

    @Override
    public String compile() {
        return null;
    }

}
