package dev.ksarou.compiler.highlevel.domain.compile;

import com.google.common.collect.ImmutableList;
import dev.ksarou.compiler.highlevel.domain.memory.Context;
import dev.ksarou.compiler.highlevel.domain.token.Tokens;

public class WhileStatementCompile extends NonTerminal {
    private final Context context;
    private final CompileToken keyword;
    private final CompileToken lefPar;
    private final CompileToken expression;
    private final CompileToken rightPar;
    private final CompileToken lefBody;
    private final CompileToken statements;
    private final CompileToken rightBody;

    public WhileStatementCompile(Tokens tokens, Context context) {
        this.context = context;
        keyword = new Terminal(tokens, context);
        lefPar = Factory.instantiate(tokens, context);
        expression = new ExpressionCompile(tokens, ImmutableList.of(")"), context);
        rightPar = Factory.instantiate(tokens, context);
        lefBody = Factory.instantiate(tokens, context);
        statements = new StatementsCompile(tokens, context);
        rightBody = Factory.instantiate(tokens, context);
    }

    @Override
    public String toXML() {
        return "<whileStatement>\n" +
                keyword.toXML() +
                lefPar.toXML() +
                expression.toXML() +
                rightPar.toXML() +
                lefBody.toXML() +
                statements.toXML() +
                rightBody.toXML() +
                "</whileStatement>\n";
    }

    @Override
    public String compile() {
        int count = context.getWhileCounter();
        return "label WHILE_EXP" + count + "\n" +
                expression.compile() +
                "not\n" +
                "if-goto WHILE_END" + count + "\n" +
                statements.compile() +
                "goto WHILE_EXP" + count + "\n" +
                "label WHILE_END" + count + "\n";
    }
}
