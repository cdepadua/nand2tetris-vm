package dev.ksarou.compiler.highlevel.domain.compile;

import com.google.common.collect.ImmutableList;
import dev.ksarou.compiler.highlevel.domain.memory.Context;
import dev.ksarou.compiler.highlevel.domain.token.Tokens;

public class LetStatementCompile extends NonTerminal {

    private final boolean array;
    private final Context context;
    private final CompileToken keyword;
    private final CompileToken name;
    private final CompileToken leftArray;
    private final CompileToken expressionArray;
    private final CompileToken rightArray;
    private final CompileToken equal;
    private final CompileToken expression;
    private final CompileToken end;

    public LetStatementCompile(Tokens tokens, Context context) {
        this.context = context;
        keyword = new Terminal(tokens, context);
        name = Factory.instantiate(tokens, context);
        array = tokens.get(0).getWord().equals("[");
        leftArray = array ? Factory.instantiate(tokens, context) : null;
        expressionArray = array ? new ExpressionCompile(tokens, ImmutableList.of("]"), context) : null;
        rightArray = array ? Factory.instantiate(tokens, context) : null;
        equal = Factory.instantiate(tokens, context);
        expression = new ExpressionCompile(tokens, ImmutableList.of(";"), context);
        end = Factory.instantiate(tokens, context);
    }

    @Override
    public String toXML() {
        return "<letStatement>\n" +
                keyword.toXML() +
                name.toXML() +
                (leftArray != null ? leftArray.toXML() : "") +
                (expressionArray != null ? expressionArray.toXML() : "") +
                (rightArray != null ? rightArray.toXML() : "") +
                equal.toXML() +
                expression.toXML() +
                end.toXML() +
                "</letStatement>\n";
    }

    @Override
    public String compile() {
        if (array) {
            return expressionArray.compile() +
                    "push " + name.compile() + "\n" +
                    "add\n" +
                    expression.compile() +
                    "pop temp 0\n" +
                    "pop pointer 1\n" +
                    "push temp 0\n" +
                    "pop that 0\n";
        }
        return expression.compile() +
                "pop " + name.compile() + "\n";
    }
}
