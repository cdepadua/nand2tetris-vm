package dev.ksarou.compiler.highlevel.domain.compile;

import dev.ksarou.compiler.highlevel.domain.memory.Context;
import dev.ksarou.compiler.highlevel.domain.memory.Variable;
import dev.ksarou.compiler.highlevel.domain.token.Tokens;

import java.util.ArrayList;
import java.util.List;

class SubroutineTermCompile extends NonTerminal {
    private final Context context;
    private final List<CompileToken> method = new ArrayList<>();
    private final CompileToken left;
    private final CompileToken expressionList;
    private final CompileToken right;

    public SubroutineTermCompile(Tokens tokens, Context context) {
        this.context = context;
        while (!tokens.getFirstWord().equals("("))
            method.add(Factory.instantiate(tokens, context));
        left = Factory.instantiate(tokens, context);
        expressionList = new ExpressionsListCompile(tokens, context);
        right = Factory.instantiate(tokens, context);
    }

    @Override
    public String toXML() {
        return "<term>\n" +
                method.stream().map(CompileToken::toXML).reduce((e1, e2) -> e1 + e2).orElse("") +
                left.toXML() +
                expressionList.toXML() +
                right.toXML() +
                "</term>\n";
    }

    @Override
    public String compile() {
        CompileToken first = method.get(0);
        if (method.size() == 1) {
            return "push pointer 0\n" +
                    expressionList.compile() +
                    "call " + context.getClassName() + "." + first.getToken().getWord() + " " +
                    (((ExpressionsListCompile) expressionList).count() + 1) + "\n";
        } else if (first.isTerminal() && context.isAVariable(first.getToken().getWord())) {
            Variable variable = context.getVariableByName(first.getToken().getWord());
            String kind = variable.getKind().equals("field") ? "this" : variable.getKind();
            return "push " + kind + " " + variable.getPosition() + "\n" +
                    expressionList.compile() +
                    "call " + variable.getType() + "." + method.get(2).compile() + " " +
                    (((ExpressionsListCompile) expressionList).count() + 1) + "\n";
        } else {
            return expressionList.compile() + "call " +
                    method.stream().map(CompileToken::compile).reduce((s1, s2) -> s1 + s2).orElse("") + " " +
                    ((ExpressionsListCompile) expressionList).count() + "\n";
        }
    }
}
