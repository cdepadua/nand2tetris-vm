package dev.ksarou.compiler.highlevel.domain.compile;

import dev.ksarou.compiler.highlevel.domain.memory.Context;
import dev.ksarou.compiler.highlevel.domain.token.Tokens;

public class TermCompile extends NonTerminal {
    private final Context context;
    private final CompileToken term;

    public TermCompile(Tokens tokens, Context context) {
        this.context = context;
        term = Factory.instantiate(tokens, context);
    }

    @Override
    public String toXML() {
        return "<term>\n" +
                term.toXML() +
                "</term>\n";
    }

    @Override
    public String compile() {
        if (term.isTerminal() && context.isAVariable(term.getToken().getWord()))
            return "push " + term.compile() + "\n";
        return term.compile();
    }
}
