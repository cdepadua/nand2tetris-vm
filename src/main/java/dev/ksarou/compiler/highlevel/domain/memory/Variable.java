package dev.ksarou.compiler.highlevel.domain.memory;

public class Variable {

    private final String kind;
    private final String type;
    private final String name;
    private final VariableScope scope;
    private final long position;

    public Variable(String kind, String type, String name, VariableScope scope, long position) {
        this.kind = kind;
        this.name = name;
        this.type = type;
        this.position = position;
        this.scope = scope;
    }

    public String getKind() {
        return kind;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public long getPosition() {
        return position;
    }

    public VariableScope getScope() {
        return scope;
    }
}
