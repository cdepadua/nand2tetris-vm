package dev.ksarou.compiler;

import dev.ksarou.compiler.lowlevel.VM;

import static dev.ksarou.wotif.core.Conditions.when;

public class App {
    public static void main(String[] args) {
        when(args).isEmpty().then(() -> {
            throw new RuntimeException("Argument must not be empty");
        });
        VM VM = new VM(args[0]);
        VM.compile();
    }
}
